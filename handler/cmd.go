package handler

import (
	"bufio"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"
	"sync"
)

type CmdWrapper struct {
	ThreadNum  int
	IPFileAddr string
	IP         string
	Port       int
	ProxyIP    string
	ProxyPort  int
	Cmd        string
}

// 若 ProxyUrl 为空则直接向 TargetUrl 发起请求
type UrlWrapper struct {
	TargetUrl string
	ProxyUrl  string
}

/*
会有多个 Url 并发请求
*/
func (w *CmdWrapper) ParseUrls() (urls []*UrlWrapper, err error) {
	urls = []*UrlWrapper{}
	httpPre := "http://"

	if len(w.IPFileAddr) == 0 {
		urlWrapper := UrlWrapper{}
		urlWrapper.TargetUrl = fmt.Sprintf("%v%v:%v/bcmd", httpPre, w.IP, w.Port)
		if len(w.ProxyIP) != 0 {
			urlWrapper.ProxyUrl = fmt.Sprintf("%v%v:%v", httpPre, w.ProxyIP, w.ProxyPort)
		}
		urls = append(urls, &urlWrapper)
		return
	}
	f, err := os.Open(w.IPFileAddr)
	if err != nil {
		return nil, err
	}
	buf := bufio.NewReader(f)
	for {
		lineBytes, _, err := buf.ReadLine()
		line := string(lineBytes)
		fmt.Printf("file %v", line)
		if err != nil && err != io.EOF {
			return urls, err
		}
		if io.EOF == err {
			break
		}

		line = strings.TrimSpace(line)

		if strings.HasPrefix(line, "#") {
			continue
		}
		if len(line) == 0 {
			continue
		}
		splits := strings.Split(line, "||")
		if len(splits) == 1 || len(splits) > 4 {
			return nil, errors.New("IP file format error")
		}
		urlWrapper := UrlWrapper{}
		urlWrapper.TargetUrl = fmt.Sprintf("%v%v:%v/bcmd", httpPre, splits[0], splits[1])

		if len(splits) == 4 {
			urlWrapper.ProxyUrl = fmt.Sprintf("%v%v:%v", httpPre, splits[2], splits[3])
		}
		urls = append(urls, &urlWrapper)
	}
	return urls, nil
}

type BcmdResp struct {
	Code   int64  `json:"code"`
	Msg    string `json:"msg"`
	Stdout string `json:"stdout"`
	Stderr string `json:"stderr,omitempty"`
	Err    string `json:"err,omitempty"`
}

type BcmdRespWrapper struct {
	SrcUrl string
	IOErr  string
	Resp   *BcmdResp
}

func (w *CmdWrapper) Execute() {
	urls, err := w.ParseUrls()
	if err != nil {
		fmt.Printf("Error:%v\n", err)
		return
	}

	threadChan := make(chan int, w.ThreadNum)
	resChan := make(chan *BcmdRespWrapper, w.ThreadNum)
	urlWg := sync.WaitGroup{}

	for _, url := range urls {
		// TODO 判断是否需要 proxy
		urlWg.Add(1)
		go func(url *UrlWrapper) {
			threadChan <- 0
			defer func() {
				urlWg.Done()
				<-threadChan
			}()
			resp, err := Post(url, w.Cmd)
			respWrapper := BcmdRespWrapper{}
			respWrapper.SrcUrl = url.TargetUrl
			if err != nil {
				respWrapper.IOErr = err.Error()
			} else {
				respWrapper.IOErr = ""
			}
			if resp == nil {
				respWrapper.Resp = &BcmdResp{}
			} else {
				respWrapper.Resp = resp
			}
			resChan <- &respWrapper
		}(url)
	}
	go func() {
		urlWg.Wait()
		close(resChan)
	}()
	for r := range resChan {
		fmt.Printf("SrcUrl:%v\n", r.SrcUrl)
		fmt.Printf("IOError:%v\n", r.IOErr)
		fmt.Printf("Code:%v\n", r.Resp.Code)
		fmt.Printf("Msg:%v\n", r.Resp.Msg)
		fmt.Printf("Stdout:\n%v\n", r.Resp.Stdout)
		fmt.Printf("Stderr:\n%v\n", r.Resp.Stderr)
	}
}

type BcmdReq struct {
	Cmd string `json:"cmd"`
}

func Post(urlWrapper *UrlWrapper, cmd string) (resp *BcmdResp, err error) {
	req := BcmdReq{}
	req.Cmd = cmd
	postJson, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}

	postReq, err := http.NewRequest("POST", urlWrapper.TargetUrl, bytes.NewBuffer(postJson))
	if err != nil {
		return nil, err
	}
	postReq.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	if len(urlWrapper.ProxyUrl) != 0 {
		proxyUrl, _ := url.Parse(urlWrapper.ProxyUrl)
		transport := &http.Transport{}
		transport.Proxy = http.ProxyURL(proxyUrl)
		client.Transport = transport
	}

	postResp, err := client.Do(postReq)
	if err != nil {
		return nil, err
	}
	defer postReq.Body.Close()

	body, _ := ioutil.ReadAll(postResp.Body)
	r := &BcmdResp{}
	json.Unmarshal([]byte(body), r)
	return r, nil
}
