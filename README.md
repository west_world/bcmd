## commit notes 2018.04.26

bcmd 客户端功能如下

- 直接指定 bagent 的 IP，执行命令
e.g. ./bcmd -i 127.0.0.1 -p 8090 -c "ls /tmp/"
- 指定 IP 列表文件，执行命令 e.g. ./bcmd -f test.ip -c "ls /tmp/"
- 多线程执行，线程之间的内容不会交叉
- 只需要制定 IP 和端口号，"http://" 和 url 路径程序会自动填充
- 输出内容包括：来源 url，IO 错误，bagent 错误，命令 exit code，标准输出，标准错误输出

## commit notes 2018.05.05

- 输出 code 和 msg
- 增加返回空值校验逻辑

## commit note 2018.05.15
修改 proxy 方式。发起请求时必须制定 bagent 的 IP 和端口号。
