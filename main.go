package main

import (
	"flag"
	"fmt"
	"os"

	"bcmd/handler"
)

func main() {

	cmdWrapper := handler.CmdWrapper{}

	flag.IntVar(&cmdWrapper.ThreadNum, "t", 1, "command executor thread num")
	flag.StringVar(&cmdWrapper.IPFileAddr, "f", "", "IPs file")
	flag.StringVar(&cmdWrapper.IP, "i", "", "IP address of bagent")
	flag.IntVar(&cmdWrapper.Port, "p", -1, "port of bagent")
	flag.StringVar(&cmdWrapper.ProxyIP, "x", "", "IP addr of proxy")
	flag.IntVar(&cmdWrapper.ProxyPort, "P", -1, "port addr of proxy")
	flag.StringVar(&cmdWrapper.Cmd, "c", "", "command")
	flag.Parse()

	if len(cmdWrapper.IP) == 0 && len(cmdWrapper.IPFileAddr) == 0 {
		fmt.Sprintf("IP or an IP file should be given")
		os.Exit(1)
	}
	if len(cmdWrapper.IP) != 0 && cmdWrapper.Port == -1 {
		fmt.Sprintf("the port should be given")
		os.Exit(1)
	}

	if len(cmdWrapper.ProxyIP) != 0 && cmdWrapper.ProxyPort == -1 {
		fmt.Sprintf("the port of proxy should be given")
		os.Exit(1)
	}

	if len(cmdWrapper.Cmd) == 0 {
		fmt.Sprintf("no command, exit")
		os.Exit(1)
	}

	if cmdWrapper.ThreadNum < 0 {
		fmt.Sprintf("thread num must > 0")
		os.Exit(1)
	}

	cmdWrapper.Execute()

	os.Exit(0)
}
